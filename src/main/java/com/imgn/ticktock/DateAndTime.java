package com.imgn.ticktock;

public class DateAndTime {
	private String employeeID;
	private String projectID;
	private String startDate;
	private String endDate;

	public DateAndTime() {

	}

	public DateAndTime(String employeeID, String projectID, String startDate, String endDate) {
		this.employeeID = employeeID;
		this.projectID = projectID;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public String getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(String employeeID) {
		this.employeeID = employeeID;
	}

	public String getProjectID() {
		return projectID;
	}

	public void setProjectID(String projectID) {
		this.projectID = projectID;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
}
