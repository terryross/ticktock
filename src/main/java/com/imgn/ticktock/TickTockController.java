package com.imgn.ticktock;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mongodb.DBCollection;

@RestController
public class TickTockController {
	@Autowired
	private TickTockService tickTockService;

	@RequestMapping(value = "y", method = { RequestMethod.POST })
	public Employee employee(@RequestBody Employee emp) throws Exception {
		tickTockService.persistEmployee(emp);

		return emp;
	}

	@RequestMapping(value = "/employees/projectManagement", method = { RequestMethod.POST })
	public Project project(@RequestBody Project proj) throws Exception {
		tickTockService.persistProject(proj);
		return proj;

	}

	@RequestMapping(value = "/employees/datesAndTimesManagement", method = { RequestMethod.POST })
	public DateAndTime dateAndTime(@RequestBody DateAndTime dat) throws Exception {
		tickTockService.persistDateAndTime(dat);

		return dat;
	}
}
