package com.imgn.ticktock;

import java.net.UnknownHostException;
import java.text.*;
import com.mongodb.*;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class TickTockService {
	public DBCollection persistEmployee(Employee emp) throws Exception {
		DB db = getConnection();
		DBCollection employeesCollection = db.getCollection("employees");
		UUID employeeID = UUID.randomUUID();
		DBObject employee = new BasicDBObject();
		employee.put("employeeID", employeeID);
		employee.put("firstName", emp.getFirstName());
		employee.put("surname", emp.getSurname());
		employeesCollection.insert(employee);
		return employeesCollection;
	}

	private DB getConnection() throws UnknownHostException {
		MongoClient mongoClient = new MongoClient();
		return mongoClient.getDB("TickTock");
	}

	public DBCollection persistProject(Project proj) throws Exception {
		DB db = getConnection();
		DBCollection projectsCollection = db.getCollection("projects");
		UUID projectID = UUID.randomUUID();
		DBObject project = new BasicDBObject();
		project.put("projectID", projectID);
		project.put("projectName", proj.getProjectName());
		projectsCollection.insert(project);
		return projectsCollection;
	}

	public DBCollection persistDateAndTime(DateAndTime dat) throws Exception {
		DB db = getConnection();
		DBCollection datesAndTimesCollection = db.getCollection("datesAndTimes");
		DBObject dateAndTime = new BasicDBObject();
		String startDatePattern = "yyyy-MM-dd HH:mm";
		SimpleDateFormat formatStartDate = new SimpleDateFormat(startDatePattern);
		String startDate = dat.getStartDate();
		Date customStartDate = formatStartDate.parse(startDate);
		String endDatePattern = "yyyy-MM-dd HH:mm";
		SimpleDateFormat formatEndDate = new SimpleDateFormat(endDatePattern);
		String endDate = dat.getEndDate();
		Date customEndDate = formatEndDate.parse(endDate);

		dateAndTime.put("employeeID", dat.getEmployeeID());
		dateAndTime.put("projectID", dat.getProjectID());
		dateAndTime.put("startDate", customStartDate);
		dateAndTime.put("endDate", customEndDate);
		datesAndTimesCollection.insert(dateAndTime);
		return datesAndTimesCollection;
	}
}
