package com.imgn.ticktock;

import java.util.*;

public class Project {
	private String projectName;

	public Project() {

	}

	public Project(String projectName) {
		this.projectName = projectName;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
}
